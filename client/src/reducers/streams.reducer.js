import {
	CREATE_STREAM,
	DELETE_STREAM,
	EDIT_STREAM,
	FETCH_STREAM,
	FETCH_STREAMS
} from '../actions/streams.types';

export default (state = {}, action) => {
	switch (action.type) {
		case FETCH_STREAMS:
			return action.payload.reduce(
				(obj, stream) => ({ ...obj, [stream['id']]: stream }),
				{}
			);
		case FETCH_STREAM:
			return { ...state, [action.payload.id]: action.payload };
		case CREATE_STREAM:
			return { ...state, [action.payload.id]: action.payload };
		case EDIT_STREAM:
			return { ...state, [action.payload.id]: action.payload };
		case DELETE_STREAM:
			const newState = { ...state };
			delete newState[action.payload];
			return { ...newState };
		default:
			return state;
	}
};
