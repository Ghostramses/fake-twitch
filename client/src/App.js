import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import StreamCreate from './components/streams/StreamCreate';
import StreamEdit from './components/streams/StreamEdit';
import StreamDelete from './components/streams/StreamDelete';
import StreamList from './components/streams/StreamList';
import StreamShow from './components/streams/StreamShow';
import Header from './components/Header';
import history from './history';

//521703503899-1lhbcp09o2tas3o1mhv4f6qcgp1nu3fq.apps.googleusercontent.com

function App() {
	return (
		<div className='ui router'>
			<Router history={history}>
				<Header />
				<Switch>
					<Route path='/' exact component={StreamList} />
					<Route path='/streams/new' exact component={StreamCreate} />
					<Route
						path='/streams/edit/:id'
						exact
						component={StreamEdit}
					/>
					<Route
						path='/streams/delete/:id'
						exact
						component={StreamDelete}
					/>
					<Route path='/streams/:id' exact component={StreamShow} />
				</Switch>
			</Router>
		</div>
	);
}

export default App;
