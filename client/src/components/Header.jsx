import React from 'react';
import { Link } from 'react-router-dom';
import GoogleAuth from './GoogleAuth';

const Header = () => {
	return (
		<div className='ui violet menu'>
			<Link to='/' className='item'>
				FakeTwitch
			</Link>
			<div className='right menu'>
				<Link className='item' to='/'>
					All Streams
				</Link>
				<GoogleAuth />
			</div>
		</div>
	);
};

export default Header;
