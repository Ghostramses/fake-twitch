import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Modal from '../Modal';
import history from './../../history';
import { fetchStream, deleteStream } from './../../actions';
import { Link } from 'react-router-dom';

const StreamDelete = props => {
	useEffect(() => {
		props.fetchStream(props.match.params.id);
	}, []);

	const actions = (
		<>
			<button
				className='ui button negative'
				onClick={() => props.deleteStream(props.match.params.id)}
			>
				Delete
			</button>
			<Link className='ui button' to='/'>
				Cancel
			</Link>
		</>
	);

	return (
		<div>
			<Modal
				title='Delete Stream'
				content={
					!props.stream
						? 'Are you sure you want to delete this stream?'
						: `Are you sure you want to delete the stream with title: ${props.stream.title}`
				}
				actions={actions}
				onDismiss={() => history.push('/')}
			/>
		</div>
	);
};

const mapStateToProps = (state, ownProps) => ({
	stream: state.streams[ownProps.match.params.id]
});

export default connect(mapStateToProps, { fetchStream, deleteStream })(
	StreamDelete
);
