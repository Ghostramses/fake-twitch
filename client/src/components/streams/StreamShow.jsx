import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { fetchStream } from '../../actions';
import flv from 'flv.js';

const StreamShow = props => {
	const useVideo = useRef(null);
	let flvPlayer = null;

	const createPlayer = () => {
		if (flvPlayer || !props.stream) return;
		flvPlayer = flv.createPlayer({
			type: 'flv',
			url: `http://localhost:8000/live/${props.match.params.id}.flv`
		});
		flvPlayer.attachMediaElement(useVideo.current);
		flvPlayer.load();
		flvPlayer.play();
	};

	useEffect(() => {
		props.fetchStream(props.match.params.id);
		createPlayer();
		return () => {
			flvPlayer.destroy();
		};
	}, []);

	useEffect(() => {
		createPlayer();
	});

	if (!props.stream) return <div>Loading...</div>;
	return (
		<div className='ui container'>
			<video ref={useVideo} style={{ width: '100%' }} controls />
			<h1>{props.stream.title}</h1>
			<h5>{props.stream.description}</h5>
		</div>
	);
};

const mapStateToProps = (state, ownProps) => ({
	stream: state.streams[ownProps.match.params.id]
});

export default connect(mapStateToProps, { fetchStream })(StreamShow);
