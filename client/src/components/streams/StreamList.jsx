import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchStreams } from './../../actions';

class StreamList extends Component {
	componentDidMount() {
		this.props.fetchStreams();
	}

	renderList = () =>
		this.props.streams.map(stream => (
			<div className='item' key={stream.id}>
				{stream.userId === this.props.userId ? (
					<div className='right floated content'>
						<Link
							to={`/streams/edit/${stream.id}`}
							className='ui button primary'
						>
							Edit
						</Link>
						<Link
							to={`/streams/delete/${stream.id}`}
							className='ui button negative'
						>
							Delete
						</Link>
					</div>
				) : null}
				<i className='large middle aligned icon camera'></i>
				<div className='content'>
					<Link to={`/streams/${stream.id}`} className='header'>
						{stream.title}
					</Link>
					<div className='description'>{stream.description}</div>
				</div>
			</div>
		));

	render() {
		return (
			<div className='ui container'>
				<h2>Streams</h2>
				<div className='ui celled list'>{this.renderList()}</div>
				{this.props.isSignedIn && (
					<div style={{ textAlign: 'right' }}>
						<Link to='/streams/new/' className='ui button primary'>
							Create Stream
						</Link>
					</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	streams: Object.values(state.streams),
	userId: state.auth.userId,
	isSignedIn: state.auth.isSignedIn
});

export default connect(mapStateToProps, { fetchStreams })(StreamList);
