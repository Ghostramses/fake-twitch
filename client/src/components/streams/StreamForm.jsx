import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

// * Componente StreamForm
class StreamForm extends Component {
	renderInput = ({ input, label, meta }) => (
		<div className={`field ${meta.error && meta.touched ? 'error' : ''}`}>
			<label>{label}</label>
			<input {...input} />
			{meta.touched && meta.error && (
				<div className='ui error message'>{meta.error}</div>
			)}
		</div>
	);

	render() {
		return (
			<div className='ui container'>
				<form
					className='ui form error'
					onSubmit={this.props.handleSubmit(this.props.onSubmit)}
				>
					<Field
						name='title'
						component={this.renderInput}
						label='Enter Title'
					/>
					<Field
						name='description'
						component={this.renderInput}
						label='Enter Description'
					/>
					<button className='ui button primary' type='submit'>
						Submit
					</button>
				</form>
			</div>
		);
	}
}

// * Validación del formulario
const validate = formValues => {
	const errors = {};
	if (!formValues.title) {
		errors.title = 'You must enter a title';
	}
	if (!formValues.description) {
		errors.description = 'You must enter a description';
	}
	return errors;
};

export default reduxForm({
	form: 'streamForm',
	validate
})(StreamForm);
