import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn, signOut } from './../actions';

class GoogleAuth extends Component {
	componentDidMount() {
		window.gapi.load('client:auth2', async () => {
			await window.gapi.client.init({
				clientId:
					'521703503899-1lhbcp09o2tas3o1mhv4f6qcgp1nu3fq.apps.googleusercontent.com',
				scope: 'email'
			});
			this.auth = await window.gapi.auth2.getAuthInstance();
			this.onAuthChange(this.auth.isSignedIn.get());
			this.auth.isSignedIn.listen(this.onAuthChange);
		});
	}

	onAuthChange = isSignedIn => {
		if (isSignedIn) {
			this.props.signIn(this.auth.currentUser.get().getId());
		} else {
			this.props.signOut();
		}
	};

	renderAuthButton = () => {
		let message = '';
		if (this.props.isSignedIn === null) return null;
		else if (this.props.isSignedIn) message = 'Sign Out';
		else message = 'Sign In With Google';
		return (
			<button
				className='ui red google button'
				onClick={
					this.props.isSignedIn
						? () => this.auth.signOut()
						: () => this.auth.signIn()
				}
			>
				<i className='google icon'></i>
				{message}
			</button>
		);
	};
	render() {
		return <div>{this.renderAuthButton()}</div>;
	}
}

const mapStateToProps = state => ({ isSignedIn: state.auth.isSignedIn });

export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);
