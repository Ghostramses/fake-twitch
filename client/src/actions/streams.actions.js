import history from '../history';
import streams from './../apis/streams';
import {
	CREATE_STREAM,
	DELETE_STREAM,
	EDIT_STREAM,
	FETCH_STREAM,
	FETCH_STREAMS
} from './streams.types';

export const createStream = formValues => async (dispatch, getState) => {
	const { userId } = getState().auth;
	const response = await streams.post('/streams', { ...formValues, userId });
	dispatch({ type: CREATE_STREAM, payload: response.data });

	history.push('/');
};

export const fetchStreams = () => async dispatch => {
	const response = await streams.get('/streams');
	dispatch({ type: FETCH_STREAMS, payload: response.data });
};

export const fetchStream = streamId => async dispatch => {
	const response = await streams.get(`/streams/${streamId}`);
	dispatch({ type: FETCH_STREAM, payload: response.data });
};

export const editStream = (streamId, formValues) => async dispatch => {
	const response = await streams.patch(`/streams/${streamId}`, formValues);
	dispatch({ type: EDIT_STREAM, payload: response.data });

	history.push('/');
};

export const deleteStream = id => async dispatch => {
	await streams.delete(`/streams/${id}`);
	dispatch({ type: DELETE_STREAM, payload: id });
	history.push('/');
};
