import { SIGN_IN, SIGN_OUT } from './auth.types';

export const signIn = userId => ({ type: SIGN_IN, payload: userId });
export const signOut = () => ({ type: SIGN_OUT });
